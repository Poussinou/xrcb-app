import React, { createContext, useContext, useEffect, useState } from "react";
import { useLoadListeners  } from "./appLoadListeners";
import { Radio, RadioJson } from "../models/radio";
import createTrie from "autosuggest-trie";
import { normalise } from "../utils";
import { getRadios } from "./api";

type TrieData = { id: number, text : string };

export default class RadiosService {
    radios : Array<Radio>
    byID : { [id : number]: Radio};
    radiosTrie: { getMatches: (query: string) => Array<TrieData>};

    static async setup() {
        const radioData = await getRadios();
        if(radioData !== null) {
            return new RadiosService(radioData.sort((a, b) => {
                return a.acf.anyo_fundacion < b.acf.anyo_fundacion ? -1 : 1;
            }));
        } else {
            return undefined;
        }
    }

    constructor(radioData: Array<RadioJson>) {
        this.radios = [];
        this.byID = {};
        let trieData : Array<TrieData> = [];
        radioData.forEach((data) => {
            const radio = new Radio(data);
            this.radios.unshift(radio);
            trieData.unshift({id: radio.id, text: radio.normalisedText() });
            this.byID[radio.id] = radio;
        })
        this.radiosTrie = createTrie(trieData, "text");
    }

    getByID(id : number) {
        return this.byID[id];
    }

    search(searchTerm : string) {
        const results : Array<TrieData> = this.radiosTrie.getMatches(normalise(searchTerm));
        return [... new Set(results.map((d) => this.byID[d.id]))].sort((a, b) => (
            a.year < b.year ? -1 : 1
        ));
    }
}
const RadiosContext = createContext(undefined as RadiosService | undefined);
const Provider = ({children} :  { children : React.ReactNode }) => {
    const loadListeners = useLoadListeners();
    const [radiosService, setRadiosService] = useState(undefined as RadiosService | undefined );
    useEffect(() => {
        const f = async () => {
          const result = await RadiosService.setup();
          if(result) {
            setRadiosService(result);
            loadListeners.loaded("radiosService");
          } else {
            loadListeners.failed("radiosService");
          }
        };
        f();
      }, []);
      return (
          <RadiosContext.Provider value={radiosService}>
              {children}
          </RadiosContext.Provider>
      );
}

const useRadiosService : () => RadiosService = () => useContext(RadiosContext)!

export { Provider, useRadiosService };