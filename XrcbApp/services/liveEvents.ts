import { getLiveEvents } from "./api";
import { LiveEvent, LiveEventJson } from "../models/liveEvent";
export default class LiveEventsService {
    events : Array<LiveEvent>;

    static async setup() {
        const liveEventsData = await getLiveEvents();
        if(liveEventsData !== null) {
            return new LiveEventsService([] || liveEventsData.reverse());
        } else {
            return null;
        }

    }

    constructor(eventsData : Array<LiveEventJson>) {
        this.events = [];
        eventsData.forEach((data) => {
            const event = new LiveEvent(data);
            this.events.unshift(event);
        });
    }

    upcoming() {
        return this.events;
    }
}