import React, { useEffect, useState } from 'react';
import {View, Text, useColorScheme } from 'react-native';

import LiveEventsService from "../services/liveEvents";
import { LiveEvent } from "../models/liveEvent";
import LiveEventComponent from "./LiveEvent";
import { COLORS,  DEFAULT_MARGIN } from '../constants';
import textStyle from "../styles/text";
import PodcastListing from './PodcastListing';
import { usePodcastsService } from '../services/podcasts';
import { t, locale } from "../services/i18n";
const LiveEvents = () => {
    const podcastsService = usePodcastsService();
    const latestPodcast = podcastsService.latest();
    const [eventsService, setEventsService] = useState(null as LiveEventsService | null);

    useEffect(() => {
        const f = async () => {
            const events = await LiveEventsService.setup();
            setEventsService(events);
        };
        f();
    }, []);

    const isDarkMode = useColorScheme() === 'dark';
    const headingStyles = {...textStyle(isDarkMode), ...{
        fontSize: 24,
        lineHeight: 36,
        fontWeight: "bold",
        paddingLeft : DEFAULT_MARGIN / 2.0,
        paddingRight : DEFAULT_MARGIN / 2.0,
        marginBottom: DEFAULT_MARGIN / 2.0,
    }}

    const wrapperStyles = {
        marginBottom: DEFAULT_MARGIN,
        flex: 1,
    }

    function renderPodcasts() {
        return (
            <View style={wrapperStyles}>
                <Text style={headingStyles}>{t("latestPodcastHeading")}</Text>
                <PodcastListing podcast={latestPodcast} style={{ paddingLeft: DEFAULT_MARGIN / 2, paddingRight: DEFAULT_MARGIN / 2 }} />
            </View>
        )
    }

    function renderEvents(events : Array<LiveEvent>) {
        return (
            <View style={wrapperStyles}>
                <Text style={headingStyles}>{t("liveEventsHeading")}</Text>
                { events.map((event) => (
                    <LiveEventComponent event={event} key={`live-event-${event.id}`}/>
                ))}
            </View>
        )
    }

    function renderLaMerce() {
        const event = new LiveEvent({
            id: "12345",
            title: "La Mercè 2022",
            date: "2022-09-24",
            start: "10:00:00",
            end: "18:00:00",
            live: true,
            radio: false,
            radio_link: false,
            podcast_link: false
        })
        return (
            <View style={wrapperStyles}>
                  <View style={{
            height: 30,
            backgroundColor: COLORS.black,
            paddingLeft: DEFAULT_MARGIN / 2,
            paddingRight: DEFAULT_MARGIN / 2
            }}>
                <Text style={{marginTop: 6,...textStyle(true)}}>
                    <Text style={{fontWeight: "bold", }}>{t("liveEventsHeading").toUpperCase() + ":"}</Text>
                    <Text>{`  ${event.metaText(locale())}`}</Text>
                </Text>
            </View>
            <LiveEventComponent event={event} key={`live-event-${event.id}`}/>
        </View>
        )
    }

    // HARD CODING LA MERCÈ event until we've done the integration properly
    return renderLaMerce();
    /* if(eventsService && eventsService.upcoming().length > 0) {
        return renderEvents(eventsService.upcoming());
    } else {
        return renderPodcasts();
    } */
}


export default LiveEvents