import React from 'react';
import {
 View,
  Text,
  SafeAreaView,
  useColorScheme,
} from 'react-native';
import { COLORS } from "../constants";
import screenStyle from "../styles/screen";
import textStyle from "../styles/text";
import { t } from "../services/i18n";


const ErrorMessage = () => {
    const isDarkMode = useColorScheme() === 'dark';
    const backgroundStyle = {
      backgroundColor: isDarkMode ? COLORS.black : COLORS.white,
      flex: 1,
    };
    return (
        <SafeAreaView style={backgroundStyle}>
             <View style={screenStyle(isDarkMode)}>
                <Text style={textStyle(isDarkMode)}>{t("errorMessage")}</Text>
             </View>
        </SafeAreaView>
    )
};

export default ErrorMessage;