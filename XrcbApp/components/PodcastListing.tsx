import React, {useState } from 'react';
import { Link } from '@react-navigation/native';
import {View, Text, TouchableOpacity, useColorScheme, Share} from "react-native";
import { useMatomo } from "matomo-tracker-react-native";
import { Podcast } from '../models/podcast';
import { usePlayerService } from '../services/player';
import { COLORS, DEFAULT_MARGIN } from "../constants";
import textStyles from "../styles/text";
import { locale, t } from "../services/i18n";
import Icon from 'react-native-ionicons';
import { Radio } from '../models/radio';

type PropsType = { podcast : Podcast,  style? : object }


const PodcastListing = ({ podcast, style={}} : PropsType) => {
    const matomo = useMatomo();
    const [playing, setPlaying] = useState(false);
    const playerService = usePlayerService();
    playerService.addTrackChangeListener((current) => {
        if(podcast.track().url == playerService.track?.url) {
            setPlaying(true);
        } else {
            setPlaying(false);
        }
    });

    const isDarkMode = useColorScheme() === 'dark';

    const onPress= async () => {
        await matomo.trackDownload({
            download: podcast.media_url
        });
        await playerService.loadTrack(podcast.track())
    }

    const onShare = () => {
        Share.share({
            url: podcast.url,
            message: podcast.url,
            title: `${t("podcastSharePrefix")} – ${podcast.title}`
        })
    }

    const styles : any = {
    container: {
        ...{
            paddingBottom: DEFAULT_MARGIN,
            borderBottomWidth: 0.5,
            paddingLeft: DEFAULT_MARGIN / 2,
            paddingRight: DEFAULT_MARGIN / 2,
            paddingTop: DEFAULT_MARGIN,
            backgroundColor: playing ? COLORS.yellow : COLORS.white
        },
        ...style
    },

    titleRow: {
        flexDirection: "row"
    },

    titleColumn: {
        flex: 0.9
    },

    shareButton: {
        flex: 0.1
    },

    title: {...{
        fontWeight: "bold"
    }, ...textStyles(isDarkMode)},

    subtitle: {...textStyles(isDarkMode), ...{
        fontSize: 12,
        marginBottom: DEFAULT_MARGIN/2,
    }},

    description: {
     ...textStyles(isDarkMode), ...{
        fontSize: 12,
        lineHeight: 16,
    }}
}
    return (
        <TouchableOpacity onPress={onPress} activeOpacity={1} style={styles.container} key={podcast.id}>
            <View style={styles.titleRow}>
                <View style={styles.titleColumn}>
                    <Text style={styles.title}>
                        {podcast.title}
                    </Text>
                    <Text style={styles.subtitle}>
                        <Link to={{ screen: "Radios", params: { screen: "Show", initial: false, params: { radioID: podcast.radio_id } } }}>
                            <Text style={{textDecorationLine: "underline"}}>{podcast.radio_name}</Text>
                        </Link>
                        – { podcast.formattedDate(locale())}
                    </Text>
                </View>
                <TouchableOpacity style={styles.shareButton} onPress={onShare} activeOpacity={1}>
                    <Icon name="share" />
                </TouchableOpacity>
            </View>
            <Text style={styles.description}>
                {podcast.description}
            </Text>
        </TouchableOpacity>
    )
}

export default PodcastListing