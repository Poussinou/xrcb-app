import React from 'react';
import { Dimensions, View, Text, useColorScheme, ImageBackground, Image } from 'react-native';

import { DEFAULT_MARGIN, COLORS } from '../constants';
import textStyle from "../styles/text";
import { LiveEvent } from "../models/liveEvent";
import { locale } from "../services/i18n";
import liveLight from "../assets/player/liveLight.png";

type Props = {
    event: LiveEvent
}

const LiveEventComponent = ({event}: Props) => {
    const imageSource = event.imageUrl || require("../assets/la_merce.jpg");
    const isDarkMode = useColorScheme() === 'dark';
    const width = Dimensions.get("window").width;
    const wrapperStyles = {
        flex: 1,
        borderTopWidth: 3,
        borderTopColor: isDarkMode ? COLORS.white : COLORS.black
    }
    const headerStyle = {
        ...textStyle(isDarkMode),
        ...{
            backgroundColor: isDarkMode ? COLORS.black : COLORS.white,
            paddingLeft: DEFAULT_MARGIN / 2,
            paddingRight: DEFAULT_MARGIN / 2,
            paddingTop: DEFAULT_MARGIN,
            paddingBottom: DEFAULT_MARGIN / 4,
            width: "auto",
            fontSize: 24,
            fontWeight: "bold"
        }
    }

    const metaStyle = {
        ...textStyle(isDarkMode),
        ...{
            backgroundColor: isDarkMode ? COLORS.black : COLORS.white,
            paddingLeft: DEFAULT_MARGIN / 2,
            paddingRight: DEFAULT_MARGIN / 2,
            paddingTop: DEFAULT_MARGIN / 4,
            paddingBottom: DEFAULT_MARGIN / 4,
            width: "auto",
            fontWeight: "bold",
        }
    }

    const lightStyle = {
        height: metaStyle.fontSize,
        width: metaStyle.fontSize,
    }

    const blurbWrapperStyle = {
        ...textStyle(isDarkMode),
        ...{
            paddingLeft: DEFAULT_MARGIN / 2,
            paddingRight: DEFAULT_MARGIN / 2,
            paddingTop: DEFAULT_MARGIN,
            paddingBottom: DEFAULT_MARGIN
        }
    }

    const metaWrapperStyle = {
        position: "absolute",
        top: 0,
        right: 0
    }

    const titleWrapperStyle = {
        position: "absolute",
        bottom: 0
    }

    const blurbTitleStyle = {
        fontWeight: "bold",
        marginBottom: DEFAULT_MARGIN / 2
    }

    return (
        <View style={wrapperStyles} key={`live-event=${event.id}`}>

            <ImageBackground source={imageSource} style={{width: width, height: width}}>
            { false && // Comment this out til we revisit after La Merce - it's shown in the header.
                <View style={metaWrapperStyle}>
                    <Text style={metaStyle}>
                        <Image style={lightStyle} source={liveLight} />
                        {` ${event.metaText(locale())}`}
                    </Text>
                </View>
            }
            <View style={titleWrapperStyle}>
                <Text style={headerStyle}>{event.radioTitle}</Text>
            </View>
            </ImageBackground>
            { (event.showTitle || event.showBlurb) &&
                <View style={blurbWrapperStyle}>
                    <Text style={blurbTitleStyle}>{event.showTitle}</Text>
                    <Text>{event.showBlurb}</Text>
                </View>
            }
        </View>
    )
}


export default LiveEventComponent