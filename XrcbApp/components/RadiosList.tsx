import React, { useEffect, useState } from 'react';
import  { FlatList, TouchableOpacity, Text, View, useColorScheme } from 'react-native';
import RadioListing from "../components/RadioListing";
import SearchInput from "../components/SearchInput"
import { Radio } from "../models/radio";
import { useRadiosService } from "../services/radios";
import { ListNavigationProps } from "../screens/Radios";
import { t } from "../services/i18n";
import { COLORS, DEFAULT_MARGIN } from '../constants';
import textStyle from "../styles/text";
import screenStyle from "../styles/screen";



type PropsType =  {
    navigation : ListNavigationProps["navigation"] }
;

const RadiosList = ({ navigation } : PropsType) => {
    const [searchTerm, setSearchTerm] = useState(undefined as string | undefined);
    const [displayedRadios, setDisplayedRadios] = useState([] as Array<Radio>);
    const radiosService = useRadiosService();
    const isDarkMode = useColorScheme() === 'dark';

    useEffect(() => {
        if(searchTerm === undefined || searchTerm == "") {
            setDisplayedRadios(radiosService.radios);
        } else if (searchTerm.length < 3) {
            // Do nothing - display what's currently displayed
        } else {
            setDisplayedRadios(radiosService.search(searchTerm));
        }
    }, [searchTerm]);

    const headingStyle = {...textStyle(isDarkMode), ...{
        marginLeft: 0,
        paddingLeft: DEFAULT_MARGIN,
        paddingRight: DEFAULT_MARGIN,
        marginTop: 0,
        paddingTop: 0,
        fontSize: 24,
        lineHeight: 36,
        marginBottom: DEFAULT_MARGIN/2,
        fontWeight: "bold",
      }}

    const renderRadio = ({item} : {item : Radio}) => {
        const onPress=()=> {
            navigation.navigate("Show", { radioID : item.id })
        }
        return (
            <TouchableOpacity onPress={onPress}  activeOpacity={1}>
                <RadioListing radio={item} />
            </TouchableOpacity>
        )
    }

    return (
        <View style={screenStyle(isDarkMode, false, false, false)}>
            <SearchInput onChange={setSearchTerm} />
            <Text style={headingStyle}>
                {
                    searchTerm === undefined || searchTerm == "" ?
                        t("radiosHeadingAll") :
                        `${displayedRadios.length} ${ displayedRadios.length == 1 ? t("radiosHeadingResultsSingular") : t("radiosHeadingResultsPlural")}`
                }
             </Text>
            <FlatList
                data={displayedRadios}
                renderItem={renderRadio}
                keyExtractor={radio => radio.id.toString()}
            />
        </View>
    )
}

export default RadiosList