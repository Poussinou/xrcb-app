import React, { useState } from 'react';
import { Track, usePlayerService } from "../services/player";
import {
  View,
} from 'react-native';

import Player from "./Player";
import { COLORS, DEFAULT_MARGIN } from "../constants";

const FloatingPlayer = () => {

  const playerService = usePlayerService();
  const [visible, setVisible] = useState(playerService.visible);

  const track = playerService.track;
  const [playingTrack, setPlayingTrack] = useState(track);

playerService.addTrackChangeListener((current) => {
  if(track !== playerService.track) {
    setPlayingTrack(playerService.track)
  }
});

  const styles = {
    position: "absolute",
    bottom: 50,
    left: 0,
    right: 0,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: COLORS.black,
    backgroundColor: playingTrack?.live ? COLORS.pink : COLORS.yellow,
    shadowColor: COLORS.black,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    padding: DEFAULT_MARGIN / 2,
    marginTop: 10,
    shadowOpacity: 0.5,
    shadowRadius: 10,
    elevation: 5,
  }

  playerService.addToggleVisibilityListener(setVisible);
  if(visible) {
    return (
      <View style={styles}>
        <Player />
      </View>
    )
  } else {
    return null;
  }
}

export default FloatingPlayer;