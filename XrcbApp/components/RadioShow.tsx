import React from 'react'
import { View, Text, useColorScheme, Dimensions, ImageBackground, ScrollView, TouchableOpacity, Share, Linking } from 'react-native';
import { DEFAULT_MARGIN, COLORS } from '../constants';
import textStyle from "../styles/text";
import { locale } from "../services/i18n";
import { useRadiosService } from "../services/radios";
import { t } from "../services/i18n";
import RadioPodcastsList from "./RadioPodcastsList";
import Icon from 'react-native-ionicons';

type PropsType = {
    radioID : number,
}

const RadioShow = ({ radioID } : PropsType ) => {
    const radiosService = useRadiosService();
    const radio = radiosService.getByID(radioID);
    const imageSource = radio.imageURL ? { uri: radio.imageURL } : require("../assets/default_radio.png");
    const isDarkMode = useColorScheme() === 'dark';
    const width = Dimensions.get("window").width;
    const wrapperStyles = {
        flex: 1,
        backgroundColor: isDarkMode ? COLORS.black : COLORS.white,
    }
    const headerStyle = {
        ...textStyle(isDarkMode),
        ...{
            backgroundColor: isDarkMode ? COLORS.black : COLORS.white,
            paddingLeft: DEFAULT_MARGIN / 2,
            paddingRight: DEFAULT_MARGIN / 2,
            paddingTop: DEFAULT_MARGIN,
            paddingBottom: DEFAULT_MARGIN / 4,
            width: "auto",
            fontSize: 24,
            fontWeight: "bold"
        }
    }

    const titleWrapperStyle = {
        position: "absolute",
        bottom: 0
    }

    const metaStyle = {
        ...textStyle(isDarkMode),
        ...{
            backgroundColor: isDarkMode ? COLORS.black : COLORS.white,
            paddingLeft: 0,
            paddingRight: 0,
            paddingTop: DEFAULT_MARGIN / 4,
            paddingBottom: DEFAULT_MARGIN,
            width: "auto",
            fontWeight: "bold",
        }
    }

    const blurbWrapperStyle = {
        ...textStyle(isDarkMode),
        ...{
            paddingLeft: DEFAULT_MARGIN / 2,
            paddingRight: DEFAULT_MARGIN / 2,
            paddingTop: DEFAULT_MARGIN,
            paddingBottom: DEFAULT_MARGIN,
        }
    }

    const subheadRowStyle = {
        flexDirection: "row",
        marginBottom: DEFAULT_MARGIN
    }

    const subheadWrapperStyle = {
        flex: 0.9, // 0.8 when RSS button uncommented
        marginTop: 1
    }

    const feedStyle = {
        flex: 0.1,
    }

    const shareStyle = {
        flex: 0.1,
    }

    const podcastsWrapperStyle = {
        ...textStyle(isDarkMode),
        ...{
            //paddingLeft: DEFAULT_MARGIN,
            //paddingRight: DEFAULT_MARGIN,
            paddingTop: 0,
            paddingBottom: DEFAULT_MARGIN
        }
    }

    const podcastsTitleStyle = {
        ...textStyle(isDarkMode),
        ...{
            paddingTop: DEFAULT_MARGIN,
            fontSize: 24,
            paddingBottom: DEFAULT_MARGIN,
            paddingLeft: DEFAULT_MARGIN/2,
            fontWeight: "bold"
        }
    }

    const onShare = () => {
        Share.share({
            url: radio.link,
            message: radio.link,
            title: `${t("radioSharePrefix")} – ${radio.title}`
        })
    }

    const onOpenRSS = () => {
        Linking.canOpenURL(radio.rss_feed).then(() => { Linking.openURL(radio.rssURL()) });
    }

    return (
        <ScrollView style={wrapperStyles} key={`radio=${radio.id}`}>
            <ImageBackground source={imageSource}  style={{width: width, height: width*2/3}}>
                <View style={titleWrapperStyle}>
                    <Text style={headerStyle}>{radio.title}</Text>
                </View>
            </ImageBackground>
            <View style={blurbWrapperStyle}>
                <View style={subheadRowStyle}>
                    <View style={subheadWrapperStyle}>
                        <Text style={metaStyle}>{radio.metaText(locale())}</Text>
                    </View>
                    {/* <TouchableOpacity style={feedStyle} activeOpacity={1} onPress={onOpenRSS}>
                        <Icon name="logo-rss" />
                    </TouchableOpacity> */ }
                    <TouchableOpacity style={shareStyle} activeOpacity={1} onPress={onShare}>
                        <Icon name="share" />
                    </TouchableOpacity>
                </View>
                <Text>{radio.blurb(locale())}</Text>
            </View>
            <View style={podcastsWrapperStyle}>
                <Text style={podcastsTitleStyle}>{t("radioPodcastsHeading")}</Text>
                <RadioPodcastsList radio={radio} />
            </View>
        </ScrollView>
    );
}

 export default RadioShow;