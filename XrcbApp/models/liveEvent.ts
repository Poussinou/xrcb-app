import { parse as dateParse, format as dateFormat } from 'date-fns';
import { ca, enGB, es } from 'date-fns/locale';

const locales = {
    "ca": ca,
    "en": enGB,
    "es": es
}

export type LiveEventJson = {
    id: string,
    date: string,
    start: string,
    end: string,
    title: string,
    live: boolean,
    radio: boolean,
    radio_link: boolean | string,
    podcast_link: boolean | string
};

export class LiveEvent {
    id: string;
    radioTitle: string;
    startTime: Date;
    endTime: Date;
    showTitle : undefined | string = undefined;
    showBlurb : undefined | string = undefined;
    imageUrl : undefined | string = undefined;

    constructor(data: LiveEventJson) {
        this.id = data.id
        this.radioTitle = data.title
        this.startTime = dateParse(`${data.date} ${data.start}`, "yyyy-MM-dd HH:mm:ss", new Date());
        this.endTime = dateParse(`${data.date} ${data.end}`, "yyyy-MM-dd HH:mm:ss", new Date());
    }

    metaText(localeName : "ca" | "en" | "es" = "ca") {
        const locale = locales[localeName];
        const date =  dateFormat(this.startTime,"do MMM yyyy", { locale: locale });
        const start = dateFormat(this.startTime, "HH'h", { locale : locale });
        const end = dateFormat(this.endTime, "HH'h", { locale: locale });
        return `${date}, ${start} – ${end}`;

    }
}