#!/usr/bin/env bash
cd ./android
./gradlew clean
./gradlew bundleRelease
bundletool build-apks --bundle=app/build/outputs/bundle/release/app-release.aab --output=app/build/outputs/bundle/release/app-release.apks --mode=universal --ks=app/timc-upload-key.keystore --ks-key-alias=timc-google-play-upload
cd app/build/outputs/bundle/release
unzip app-release.apks
open .
cd ../../../../../../

